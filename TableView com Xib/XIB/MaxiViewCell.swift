//
//  MaxiViewCell.swift
//  TableView com Xib
//
//  Created by Willian Antunes on 04/07/2018.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

class MaxiViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var ivMaxi: UIImageView!
    @IBOutlet weak var lbDialog: UILabel!
    @IBOutlet weak var vDialog: UIView!
    
    
    var dialogText: String = "" {
        didSet {
           updateDialog()
        }
    }
    
    
    func updateCustomLayer() {
        vDialog.layer.cornerRadius = 20
        ivMaxi.layer.borderWidth = 1.0
        ivMaxi.layer.masksToBounds = false
        ivMaxi.layer.borderColor = UIColor(red:90/255, green:145/255, blue:219/255, alpha: 1).cgColor
        ivMaxi.layer.cornerRadius = ivMaxi.frame.size.height/2
        ivMaxi.clipsToBounds = true
        ivMaxi.layer.cornerRadius = ivMaxi.frame.size.height/2
    }
    
   
    private func updateDialog() {
        lbDialog.text = dialogText
        
        //recalcular tamanho da view
    }
    
}
