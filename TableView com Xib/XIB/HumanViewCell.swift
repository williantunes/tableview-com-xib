//
//  HumanViewCell.swift
//  TableView com Xib
//
//  Created by Willian Antunes on 04/07/2018.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

class HumanViewCell: UITableViewCell {

    
    @IBOutlet weak var ivHuman: UIImageView!
    @IBOutlet weak var lbDialogHuman: UILabel!
    @IBOutlet weak var vDialogHuman: UIView!
    
    
    var dialogText: String = "" {
        didSet {
            updateDialog()
        }
    }
    
    
    func updateCustomLayer() {
        vDialogHuman.layer.cornerRadius = 20
        ivHuman.layer.borderWidth = 1.0
        ivHuman.layer.masksToBounds = false
        ivHuman.layer.borderColor = UIColor(red:90/255, green:145/255, blue:219/255, alpha: 1).cgColor
        ivHuman.layer.cornerRadius = ivHuman.frame.size.height/2
        ivHuman.clipsToBounds = true
        ivHuman.layer.cornerRadius = ivHuman.frame.size.height/2
    }
    
    private func updateDialog() {
        lbDialogHuman.text = dialogText
        
        //recalcular tamanho da view
    }
    
    
}
