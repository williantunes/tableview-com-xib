//
//  BtnSimNaoViewCell.swift
//  TableView com Xib
//
//  Created by Willian Antunes on 05/07/2018.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

enum BtnSimNaoOptionsType: String {
    case sim = "SIM"
    case nao = "NAO"
}

class BtnSimNaoViewCell: UITableViewCell {
    
    @IBOutlet weak var btnSim: UIButton!
    @IBOutlet weak var btnNao: UIButton!
    
    
    var handler: ((_ option: BtnSimNaoOptionsType) -> Void)?
    
    func updateLayerBtn() {
        btnSim.layer.cornerRadius = 15
        btnSim.layer.borderWidth = 2.0
        btnSim.layer.borderColor = UIColor(red:255/255, green:45/255, blue:85/255, alpha: 1).cgColor

        btnNao.layer.cornerRadius = 15
        btnNao.layer.borderWidth = 2.0
        btnNao.layer.borderColor = UIColor(red:255/255, green:45/255, blue:85/255, alpha: 1).cgColor
    }
    
    
    // MARK: - Action
    @IBAction func btnSim(_ sender: UIButton) {

        if sender.tag == 0 {
            handler?(BtnSimNaoOptionsType.sim)
        } else {
            handler?(BtnSimNaoOptionsType.nao)
        }
    }
}
