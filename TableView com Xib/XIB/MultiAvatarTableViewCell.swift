
import UIKit

class MultiAvatarTableViewCell: UITableViewCell {

    @IBOutlet weak private var collectionView: UICollectionView!
    
    let avatarList = AvatarData.load().self
    
    var identificador = "cell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "AvatarCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "cell")
    }
}
// MARK: - Extension
extension MultiAvatarTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return avatarList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identificador, for: indexPath) as! AvatarCollectionViewCell
        
        let model: AvataresModel = avatarList[indexPath.row]
       
        cell.btnImageAvatar.setImage(UIImage(named: model.image), for: .normal)
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 45, height: 45)
    }
}
