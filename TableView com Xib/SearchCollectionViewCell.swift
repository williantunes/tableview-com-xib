//
//  SearchCollectionViewCell.swift
//  TableView com Xib
//
//  Created by Willian Antunes on 12/07/2018.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

protocol SearchCollectionViewCellDelegate: class {
    
    func cell(_ cell: SearchCollectionViewCell, didPress button: UIButton)
}


class SearchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewFundo: UIView!
    @IBOutlet weak var viewNome: UIView!
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var buttonLembretes: UIButton!
    @IBOutlet weak var buttonMapas: UIButton!
    
    func updateLayer() {
        
        // View de Fundo
        viewFundo.layer.cornerRadius = 12
        viewFundo.layer.borderWidth = 1.0
        viewFundo.layer.borderColor = UIColor(red:241/255, green:47/255, blue:85/255, alpha: 1).cgColor
        
        // View Nome
        viewNome.layer.cornerRadius = 12
        viewNome.layer.borderColor = UIColor(red:241/255, green:47/255, blue:85/255, alpha: 1).cgColor
        
        // View buttonLembretes
        buttonLembretes.layer.cornerRadius = 12
        
        // View buttonMapas
        buttonMapas.layer.cornerRadius = 12
        
        
    }
    
    
    
    weak var delegate: SearchCollectionViewCellDelegate?
    
    @IBAction func didPress(_ sender: UIButton) {
        delegate?.cell(self, didPress: sender)
    }
    
}
