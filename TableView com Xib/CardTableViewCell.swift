//
//  CardTableViewCell.swift
//  TableView com Xib
//
//  Created by Willian Antunes on 12/07/2018.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit


protocol CardTableViewCellDelegate: class {

    func cell(_ cell: SearchCollectionViewCell, didPress button: UIButton)
}

class CardTableViewCell: UITableViewCell, SearchCollectionViewCellDelegate {
    var numberOfItems: Int = 0 { didSet{ collectionView.reloadData()  }}
    
    weak var delegate: CardTableViewCellDelegate?

    @IBOutlet weak var collectionView: UICollectionView!

    var identificador = "cell"
    var names: [String] = []

    override func awakeFromNib() {
        super.awakeFromNib()

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "SearchCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "cell")
    }
    
    func cell(_ cell: SearchCollectionViewCell, didPress button: UIButton) {
        delegate?.cell(cell, didPress: button)
    }
}

// MARK: - Extension
extension CardTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identificador, for: indexPath) as! SearchCollectionViewCell


        cell.delegate = self
        cell.updateLayer()
        if names.count > 0 {
            cell.nomeLabel.text! = names[indexPath.row]
        }
        //let model: AvataresModel = avatarList[indexPath.row]


        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 166, height: 280)
    }
}

