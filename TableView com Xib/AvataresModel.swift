//
//  AvataresModel.swift
//  TableView com Xib
//
//  Created by Willian Antunes on 10/07/2018.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import Foundation

struct AvataresModel {
    var nome: String
    var image: String
    var cor: String
}

struct AvatarData {
    
    static func load() -> [AvataresModel]{
        
        var avatares: [AvataresModel] = []
        
        avatares.append(AvataresModel(nome: "cachorro", image: "AvatarMaxi", cor: ""))
        avatares.append(AvataresModel(nome: "gato", image: "AvatarGato", cor: ""))
        avatares.append(AvataresModel(nome: "coelho", image: "AvatarCoelho", cor: ""))
        avatares.append(AvataresModel(nome: "tartatruga", image: "AvatarTartaruga", cor: ""))
        avatares.append(AvataresModel(nome: "ave", image: "AvatarAve", cor: ""))
        avatares.append(AvataresModel(nome: "peixe", image: "AvatarPeixe", cor: ""))
        
        return avatares
    }
}
