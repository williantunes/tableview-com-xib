//
//  DialogViewController.swift
//  TableView com Xib
//
//  Created by Willian Antunes on 11/07/2018.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox
import ApiAI
import MapKit
import EventKit


enum DialogType: String{
    case maxi = "MAXI"
    case user = "USER"
    case acao = "ACAO"
    case avatar = "AVATAR"
    case card = "CARD"
}


struct Dialog {
    var text: String
    var type: DialogType
}

class DialogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, myTableViewDelegate, CardTableViewCellDelegate {
    
    @IBOutlet weak var tableView: myTableView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var dateReminder: Date = Date.init()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let regionRadius: CLLocationDistance = 12000
    var searchItems: [MKMapItem] = []
    let locationManager = CLLocationManager()
    
    var dialogItens: [Dialog] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.touchDelegate = self
        
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        // Teclado - Antes
        NotificationCenter.default.addObserver(self, selector: #selector(DialogViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DialogViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // Teclado - Depois
        NotificationCenter.default.addObserver(self, selector: #selector(DialogViewController.keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DialogViewController.keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
        
        xibMax()
        xibHumano()
        xibAcao()
        xibAvatar()
        xibCard()
        
        startDialogs()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        checkLocationAuthorizationStatus()
    }
    
    @IBOutlet weak var toobar: UIToolbar!
    @IBOutlet weak var keyboardConstraint: NSLayoutConstraint!
    
    // MARK: - Teclado
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            keyboardConstraint.constant = -keyboardSize.size.height
            toobar.setNeedsLayout()
            toobar.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardConstraint.constant = 0
        toobar.setNeedsLayout()
        toobar.layoutIfNeeded()
    }
    
    @objc func keyboardDidShow() {
        
        let indexPath = IndexPath(row: dialogItens.count - 1, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    @objc func keyboardDidHide() {
        
        let indexPath = IndexPath(row: dialogItens.count - 1, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    // MARK: - Atualiza o dialogo
    /*************************************/
    
    func startDialogs() {
        dialogItens.append(Dialog(text: "Olá, em que posso ajudar?", type: .maxi))
//        dialogItens.append(Dialog(text: "Já possui uma conta?", type: .maxi))
        //dialogItens.append(Dialog(text: "Feliz em estar aqui para usar o LuvPe!", type: .user))
    }
    
    
    // MARK: - Xibs
    /*************************************/
    
    func xibMax() {
        let nibName = UINib(nibName: "MaxiViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "maxiIdentifier")
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func xibHumano() {
        let nibName = UINib(nibName: "HumanViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "humanoIdentifier")
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func xibAcao() {
        let nibName = UINib(nibName: "BtnSimNaoViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "btnIdentifier")
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func xibAvatar() {
        let nibName = UINib(nibName: "MultiAvatarTableViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "cell")
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func xibCard() {
        let nibName = UINib(nibName: "CardTableViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "cell")
        tableView.rowHeight = UITableView.automaticDimension
        
    }
    
//    func cell(_ cell: CardTableViewCell, didPress button: UIButton) {
//        print("apertou")
//    }
    
    
    // MARK: - Table view data source
    /*************************************/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dialogItens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dialogItem = dialogItens[indexPath.row]
        let type = DialogType.init(rawValue: dialogItem.type.rawValue)!
        
        switch type {
            
            case .maxi:
                let cell = tableView.dequeueReusableCell(withIdentifier: "maxiIdentifier", for: indexPath) as! MaxiViewCell
                cell.dialogText = dialogItem.text
                cell.ivMaxi.image = UIImage(named: "maxi")
                cell.updateCustomLayer()
                return cell
            
            case .user:
                let cell = tableView.dequeueReusableCell(withIdentifier: "humanoIdentifier" , for: indexPath) as! HumanViewCell
                cell.dialogText = dialogItem.text
                cell.ivHuman.image = UIImage(named: "Avatar.jpg")
                cell.updateCustomLayer()
                
                return cell
            
            case .acao:
                let cell = tableView.dequeueReusableCell(withIdentifier: "btnIdentifier" , for: indexPath) as! BtnSimNaoViewCell
                
                cell.handler = { [weak self] option in
                    if let optionSelected = BtnSimNaoOptionsType.init(rawValue: option.rawValue) {
                        switch optionSelected {
                        case .sim:
                            self?.btnSim()
                        case .nao:
                            self?.btnNao()
                        }
                    }
                }
                cell.updateLayerBtn()
                return cell
            
            case .avatar:
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell" , for: indexPath) as! MultiAvatarTableViewCell
                return cell
            
            case .card:
                var cores : [UIColor] = [.black, .orange, .red, .green, .purple]
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell" , for: indexPath) as! CardTableViewCell
    
                cell.delegate = self
                
                var names: [String] = []
                for item in searchItems {
                    names.append(item.name!)
                }
                cell.names = names
                cell.numberOfItems = searchItems.count
                return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let dialogItem = dialogItens[indexPath.row]
        let type = DialogType.init(rawValue: dialogItem.type.rawValue)!
        
        switch type {
        case .card:
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        default: break
        }
    }
    
    func cell(_ cell: SearchCollectionViewCell, didPress button: UIButton) {
        var currentItem: MKMapItem = MKMapItem.init()
        for item in searchItems {
            if item.name == cell.nomeLabel.text! {
                currentItem = item
            }
        }
        if button.titleLabel?.text! == "Abrir Mapas" {
            currentItem.openInMaps(launchOptions: nil)
        } else if button.titleLabel?.text! == "Criar Lembrete" {
            if appDelegate.eventStore == nil {
                appDelegate.eventStore = EKEventStore()
                
                appDelegate.eventStore?.requestAccess(
                    to: EKEntityType.reminder, completion: {(granted, error) in
                        if !granted {
                            print("Access to store not granted")
                            print(error?.localizedDescription)
                        } else {
                            print("Access granted")
                        }
                })
            }
            
            if (appDelegate.eventStore != nil) {
                self.createReminder(placeName: currentItem.name!)
            }
        }
    }
    
    func createReminder(placeName: String) {
        
        let reminder = EKReminder(eventStore: appDelegate.eventStore!)
        
        reminder.title = "Veterinário na " + placeName
        reminder.calendar = appDelegate.eventStore!.defaultCalendarForNewReminders()
        let date = self.dateReminder
        let alarm = EKAlarm(absoluteDate: date)
        
        reminder.addAlarm(alarm)
        
        do {
            try appDelegate.eventStore?.save(reminder,
                                             commit: true)
        } catch let error {
            print("Reminder failed with error \(error.localizedDescription)")
        }
        
        let alert = UIAlertController(title: "Lembrete Criado", message: "Seu lembrete foi criado com sucesso.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
        
    }

    
    
    //MARK: - Métodos proprios
    func btnAcao() {
        dialogItens.append(Dialog(text: "Acao", type: .acao))
        tableView.reloadData()
    }
    
    func btnSim() {
        
        AudioServicesPlaySystemSound(SystemSoundID(1004))
        
        // Atualiza o model
        dialogItens.append(Dialog(text: "Que bom ver você por aqui novamente 😉", type: .maxi))
        
        // Cria um ciclo de atualização da table view (beginUpdates() - endUpdates())
        self.tableView.beginUpdates()
        
        // Insere nova linha
        let indexPath1 = IndexPath(row: self.dialogItens.count - 1, section: 0)
        self.tableView.insertRows(at: [indexPath1], with: .automatic)
        
        // Fim da atualização
        self.tableView.endUpdates()
        
        // Scrolla pra nova celula
        self.tableView.scrollToRow(at: indexPath1, at: .bottom, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            self.dialogItens.append(Dialog(text: "Qual seu e-mail?", type: .maxi))
            self.tableView.beginUpdates()
            let indexPath2 = IndexPath(row: self.dialogItens.count - 1, section: 0)
            self.tableView.insertRows(at: [indexPath2], with: .automatic)
            self.tableView.endUpdates()
            self.tableView.scrollToRow(at: indexPath2, at: .bottom, animated: true)
        }
        //dialogItens.append(Dialog(text: "Eduardo", type: .user))
    }
    
    func btnNao() {
        dialogItens.append(Dialog(text: "Legal, vamos começar então!", type: .maxi))
        dialogItens.append(Dialog(text: "Escolha o meu Avatar para te acompanhar dentro do LuvPet.", type: .maxi))
        AudioServicesPlaySystemSound(SystemSoundID(1004))
        tableView.reloadData()
        selecionaAvatar()
    }
    
    func selecionaAvatar() {
        dialogItens.append(Dialog(text: "Avatar", type: .avatar))
        tableView.reloadData()
    }
    
    func tocouNaTabela() {
        print("Chegou na myTableView")
        textField.resignFirstResponder()
    }
    
    //MARK: - Send message
    
    @IBAction func touchSendButton(_ sender: UIButton) {
        
        let request = ApiAI.shared()?.textRequest()
        
        if let text = self.textField.text, text != "" {
            request?.query = text
            dialogItens.append(Dialog(text: self.textField.text!, type: .user))
        } else { return }
        request?.lang = "pt-BR"
        
        var intent = ""
        
        request?.setMappedCompletionBlockSuccess({ (request, response) in
            let response = response as! AIResponse
            if response.result.metadata.intentName != nil {
                intent = response.result.metadata.intentName
            }
            var entity = ""
            if let parameters = response.result.parameters! as? Dictionary<String, AIResponseParameter> {
                if parameters["date-time1"]?.dateValue != nil {
                    self.dateReminder = parameters["date-time1"]!.dateValue
                }
                if parameters["Ration"]?.stringValue != "" && parameters["Ration"]?.stringValue != nil {
                    entity = "Ração"
                }
                if parameters["Vet"]?.stringValue != "" && parameters["Vet"]?.stringValue != nil {
                    entity = "Veterinário"
                }
                if parameters["Service"]?.stringValue != "" && parameters["Service"]?.stringValue != nil {
                    entity = "Banho"
                }
            }
            if let textResponse = response.result.fulfillment.speech {
                UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseInOut, animations: {
                    var searchString = "Veterinario"
                    if intent == "Search Product" {
                        if entity == "Ration" {
                            searchString = "Agropecuaria"
                        } else if entity == "Products" {
                            searchString = "Pet Shop"
                        } else {
                            searchString = "Cafe"
                        }
                    }   else if intent == "Search Service" {
                        if entity == "Veterinário" {
                            searchString = "Veterinário"
                        } else if entity == "Banho" {
                            searchString = "Pet Shop"
                        }
                    }
                    let searchRequest = MKLocalSearch.Request()
                    searchRequest.naturalLanguageQuery = searchString
                    let region = MKCoordinateRegion(center: self.locationManager.location!.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
                    searchRequest.region = region
                    let search = MKLocalSearch(request: searchRequest)
                    search.start { response, _ in
                        guard let response = response else {
                            return
                        }
                        self.searchItems = response.mapItems
                        self.dialogItens.append(Dialog(text: textResponse, type: .maxi))
                        self.dialogItens.append(Dialog(text: self.searchItems[0].name!, type: .card))
                        self.tableView.reloadData()
                    }
                    
                    
                    
                }, completion: nil)
            }
        }, failure: { (request, error) in
            print(error!)
        })
        
        ApiAI.shared()?.enqueue(request)
        textField.text = ""
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
}
//-------------------------------------------------------------
protocol myTableViewDelegate {
    func tocouNaTabela()
}

extension myTableViewDelegate {
    func tocouNaTabela() { }
}

class myTableView: UITableView {
    
    var touchDelegate: myTableViewDelegate?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchDelegate?.tocouNaTabela()
        super.touchesBegan(touches, with: event)
    }
}

//-------------------------------------------------------------
